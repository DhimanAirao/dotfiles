local status, icons = pcall(require, "nvim-web-devicons")
if not status then
	return
end

icons.set_icon({
	["Makefile"] = { icon = " ", color = "#d45211", name = "Makefile" },
	[".md"] = { icon = " ", color = "#3582db", name = "ReadmeFile" },
	["README"] = { icon = " ", color = "#3582db", name = "Readme" },
})
