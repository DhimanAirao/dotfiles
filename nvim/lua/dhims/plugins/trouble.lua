local status, trouble = pcall(require, "trouble")
if not status then
    return
end

trouble.setup({
	auto_open = false,
	auto_close = true,
	auto_preview = true,
	auto_fold = false,
	use_diagnostic_signs = true,
})

vim.api.nvim_set_keymap("n", "<leader>tr", "<cmd>TroubleToggle<cr>", { noremap = true, silent = true })
