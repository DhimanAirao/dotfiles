local status, neogen = pcall(require, "neogen")
if not status then
	return
end

neogen.setup({
	enabled = true,
	snippet_engine = "luasnip",
	input_after_comment = true,
	languages = {
		c = {
			template = {
				annotation_convention = "doxygen",
			},
		},
	},
})
