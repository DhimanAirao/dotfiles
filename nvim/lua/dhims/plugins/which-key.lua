local status, whichkey = pcall(require, "which-key")
if not status then
	return
end

whichkey.setup({
	plugins = {
		spelling = { enabled = true },
		presets = { operators = true },
	},
	window = {
		border = "rounded",
	},
	disable = { filetypes = { "TelescopePrompt" } },
})
