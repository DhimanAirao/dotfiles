local status, wilder = pcall(require, "wilder")
if not status then
	return
end

wilder.setup({
	modes = { ":", "/", "?" },
})

wilder.set_option("use_python_remote_plugin", 0)

wilder.set_option("pipeline", {
	wilder.branch(
		wilder.cmdline_pipeline({
			use_python = 0,
			fuzzy = 1,
			fuzzy_filter = wilder.lua_fzy_filter(),
		}),
		wilder.vim_search_pipeline({
			use_python = 0,
			fuzzy = 1,
			fuzzy_filter = wilder.lua_fzy_filter(),
		}),
		wilder.history()
	),
})
local fg_color = "#c23c7a"
--local gruvbox_bg_color = "#fbf1c7"

local create_highlight = function(type)
	local ret = {
		accent = wilder.make_hl("WilderAccen" .. type, "PMenu", {
			{ a = 1 },
			{ a = 1 },
			{ foreground = fg_color, bold = true },
		}),
	}

	return ret
end

local highlighters = {
	wilder.pcre2_highlighter(),
	wilder.lua_fzy_highlighter(),
}

local popupmenu_renderer = wilder.popupmenu_renderer(wilder.popupmenu_palette_theme({
	-- 'single', 'double', 'rounded' or 'solid'
	-- can also be a list of 8 characters, see :h wilder#popupmenu_palette_theme() for more details
	border = "rounded",
	empty_message = wilder.popupmenu_empty_message_with_spinner(),
	highlighter = highlighters,
	highlights = create_highlight("Popup"),
	max_height = "75%", -- max height of the palette
	min_height = 0, -- set to the same as 'max_height' for a fixed height window
	prompt_position = "top", -- 'top' or 'bottom' to set the location of the prompt
	pumblend = 30,
	reverse = 0, -- set to 1 to reverse the order of the list, use in combination with 'prompt_position'
	left = {
		" ",
		wilder.popupmenu_devicons(),
		wilder.popupmenu_buffer_flags({
			flags = " a + ",
			icons = { ["+"] = "", a = "", h = "" },
		}),
	},
	right = {
		" ",
		wilder.popupmenu_scrollbar(),
	},
}))

local wildmenu_renderer = wilder.wildmenu_renderer({
	highlighter = highlighters,
	highlights = create_highlight("Mini"),
	--separator = " · ",
	left = { " ", wilder.wildmenu_spinner() },
	right = { " ", wilder.wildmenu_index() },
})

wilder.set_option(
	"renderer",
	wilder.renderer_mux({
		[":"] = popupmenu_renderer,
		["/"] = wildmenu_renderer,
		substitute = wildmenu_renderer,
	})
)
