-- import nvim-treesitter plugin safely
local status, treesitter = pcall(require, "nvim-treesitter.configs")
if not status then
  print("No treesitter")
  return
end

-- configure treesitter
treesitter.setup({
  -- enable syntax highlighting
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
  },
  -- enable indentation
  indent = { enable = true },
  incremental_selection = { enable = true }, 
  -- enable autotagging (w/ nvim-ts-autotag plugin)
  autotag = { enable = true },
  -- ensure these language parsers are installed
  ensure_installed = {
    "arduino",
    "bash",
    "c",
    "cpp",
    "gitignore",
    "json",
    "lua",
    "make",
    "markdown",
    "python",
    "vim",
  },
  -- auto install above language parsers
  auto_install = true,
  rainbow = { enable = true, extended_mode = true, max_file_lines = nil },
  context_commentstring = {
    enable = true,
    enable_autocmd = false,
  },
})
