local status, notify = pcall(require, "notify")
if not status then
  return
end

vim.opt.termguicolors = true

notify.setup({})
vim.notify = notify