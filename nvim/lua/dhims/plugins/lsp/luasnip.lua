local status, luasnip = pcall(require, "luasnip")
if not status then
  return
end

for _, load_type in ipairs { "vscode", "snipmate", "lua" } do
    local loader = require("luasnip.loaders.from_" .. load_type)
    loader.lazy_load()
end

luasnip.setup({})