-- import lualine plugin safely
local status, lualine = pcall(require, "lualine")
if not status then
  return
end

-- get lualine nightfly theme
local lualine_theme = require("lualine.themes.onedark")

-- new colors for theme
local colors = {
  my_bg = "#282C34",
  red = "#fb4934",
  blue = "#65D1FF",
  green = "#3EFFDC",
  violet = "#FF61EF",
  yellow = "#FFDA7B",
  orange = "#fe8019",
  black = "#000000",
  white = "#FFFFFF",
}

local hide_in_width = function()
	return vim.fn.winwidth(0) > 80
end

local diagnostics = {
	"diagnostics",
	sources = { "nvim_diagnostic", "coc" },
	sections = { "error", "warn", "info" },
	symbols = { error = " ", warn = " ", info = " " },
	diagnostics_color = {
		error = { fg = colors.red },
		warn = { fg = colors.yellow },
		info = { fg = colors.blue },
		hint = { fg = colors.green },
	},
	update_in_insert = false,
	always_visible = true,
}

local diff = {
	"diff",
	colored = true,
	symbols = {
		add = " ",
		modified = " ",
		remove = " ",
		added = " ",
	}, -- changes diff symbols
}

local filetype = {
	"filetype",
	icons_enabled = true,
	icon = nil,
  colored = true,
	fmt = function(str)
		if not (str == nil or str == "") then
			if not (str == "markdown") then
				return "." .. str
			else
				return ".md"
			end
		else
			return 'Open a file with ":e"'
		end
	end,
}

local branch = {
	"branch",
	icons_enabled = true,
	fmt = function(str)
		if str == nil or str == "" then
			local mode = vim.fn.mode()
			if mode == "n" then
				return ":)"
			elseif mode == "i" then
				return ":O"
			elseif mode == "v" then
				return ":v"
			elseif mode == "V" then
				return ":V"
			elseif mode == "" then
				return ":"
			elseif mode == "R" then
				return "-_-"
			elseif mode == "t" then
				return ";T"
			else
				return "'w'"
			end
		else
			return "git:" .. str
		end
	end,
}

local spaces = function()
	return "spaces: " .. vim.api.nvim_buf_get_option(0, "shiftwidth")
end

local function starts_with(str, start)
	return str:sub(1, #start) == start
end

local filename = {
	"filename",
  color = {gui = "italic"},
	symbols = {
		readonly = "",
		modified = "",
		unreadable = "",
		new = "",
	},
	path = 1,
	fmt = function(str)
		if not (str == "n") then
			if not (starts_with(str, "~\\")) then
				return "~/" .. str .. ""
			else
				return str
			end
		else
			return "[No Name]"
		end
	end,
  component_separators = '',
}

local filesize = {
	"filesize",
	color = { fg = colors.orange, gui = "bold" },
	cond = function()
		return vim.fn.empty(vim.fn.expand("%:t")) ~= 1
	end,
	icons_enabled = false,
}

local progress = {
	"progress",
	color = { gui = "bold" },
	fmt = function(str)
		if not (str == "Top" or str == "Bot") then
			return str
		else
			if str == "Bot" then
				return "EOF"
			elseif str == "Top" then
				return "BOF"
			end
		end
	end,
}

local encoding = {
	"encoding",
	color = { gui = "bold" },
	fmt = function(str)
		return string.upper(str)
	end,
}

local lsp_name = function()
	local msg = "No Active Lsp"
	local buf_ft = vim.api.nvim_buf_get_option(0, "filetype")
	local clients = vim.lsp.get_active_clients()
	if next(clients) == nil then
		return msg
	end
	for _, client in ipairs(clients) do
		local filetypes = client.config.filetypes
		if filetypes and vim.fn.index(filetypes, buf_ft) ~= -1 then
			return client.name
		end
	end
	return msg
end


-- change nightlfy theme colors
lualine_theme.normal.a.bg = colors.blue
lualine_theme.insert.a.bg = colors.green
lualine_theme.visual.a.bg = colors.violet
lualine_theme.command.a.bg = colors.yellow

-- configure lualine with modified theme
lualine.setup({
  options = {
    icons_enabled = true,
    theme = lualine_theme,
    disabled_filetypes = {"alpha","dashboard", "NvimTree"},
  },
	sections = {
		lualine_a = { branch },
		lualine_b = { "mode" },
		lualine_c = {filesize, filetype, progress },
		lualine_x = { diagnostics },
		lualine_y = { {
			lsp_name,
			icon = " LSP:",
			color = { fg = colors.blue, gui = "bold" },
		}, encoding },
		lualine_z = { "location" },
	},
	inactive_sections = {
		lualine_a = {},
		lualine_b = {},
		lualine_c = {},
		lualine_x = {},
		lualine_y = {},
		lualine_z = {},
	},
	winbar = {
		lualine_a = {'fileformat', filename},
		lualine_b = {},
		lualine_c = {},
		lualine_x = {},
		lualine_y = {},
		lualine_z = {},
	},
  always_divide_middle = true,
	extensions = {'nvim-tree', 'fzf',},
  dependencies = { 'nvim-tree/nvim-web-devicons' },
})
