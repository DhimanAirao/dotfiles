#!/usr/bin/sh

doas pacman -Syu
doas pacman -S --noconfirm \
    ttf-fira-code micro btop\
    exa bat less bottom go gtk4\
    diff-so-fancy kitty gtk2 gtk3\
    neovim ripgrep tmux zsh vlc \
    fzf nodejs rsync upower acpi\
    opendoas jq playerctl dunst xsel \
    maim light pulseaudio rofi xclip \
    thunar base-devel wireless_tools \
    bluez bluez-utils pulseaudio-bluetooth \
    blueman sddm npm gvfs tumbler


#install yay
git clone https://aur.archlinux.org/yay.git /tmp/yay
cd /tmp/yay
makepkg -si

yay -S --noconfirm \
    spotify awesome-git breeze-obsidian-cursor-theme \
    kripton-theme-git papirus-icon-theme \
    xf86-input-libinputc ttf-nerd-fonts-symbols \
    ttf-nerd-fonts-symbols-common ttf-firacode-nerd\
    ttf-nerd-fonts-symbols-mono sddm-sugar-candy

git config --global core.pager "diff-so-fancy | less --tabs=4 -RFX"
git config --global interactive.diffFilter "diff-so-fancy --patch"


#Configure zsh
mkdir -p ~/.config/zsh/
mkdir -p ~/.local/share/zsh
cp -vr ../zsh ~/.config/
cp -v ../cfg/xprofile ~/.xprofile

#Configure bottom terminal system monitor
mkdir -p ~/.config/bottom/
cp -v ../cfg/my_bottom.toml ~/.config/bottom/bottom.toml

#Configure micro editor
mkdir -p ~/.config/micro/colorschemes
cp -v ../cfg/my_theme.micro ~/.config/micro/colorschemes/

#Configure NeoVim
mkdir -p ~/.config/nvim/
cp -vr ../nvim/* ~/.config/nvim/

#Configure kitty terminal
mkdir -p ~/.config/kitty/
cp -vr ../kitty/* ~/.config/kitty/

#Configure tmux
cp -v ../cfg/tmux_conf ~/.tmux.conf
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

#Configure Rofi
mkdir -p ~/.config/rofi
cp -vr ../rofi ~/.config/

wget -q https://gitlab.com/DhimanAirao/dotfiles/-/snippets/2515022/raw/main/spotify.desktop -O ../cfg/spotify.desktop
doas cp ../cfg/spotify.desktop /usr/share/applications/spotify.desktop

#Configure GTK
mkdir -p ~/.config/gtk-3.0
mkdir -p ~/.config/gtk-2.0
cp -v ../cfg/gtk3 ~/.config/gtk-3.0/settings.ini
cp -v ../cfg/gtk2 ~/.config/gtk-2.0/gtkrc

#Configure cursor icons theme
mkdir -p ~/.icons/default/
cp -v ../cfg/index.theme ~/.icons/default/index.theme

#Configure touchpad
doas cp -v ../cfg/*.conf /etc/X11/xorg.conf.d/

#Configure bluetooth
doas systemctl enable bluetooth.service

#Configure Awesome WM
git clone --recursive https://gitlab.com/DhimanAirao/awesomewm.git -b staging ../cfg/awesomewm
mkdir -p ~/.config/awesome
cp -r ../cfg/awesomewm/* ~/.config/awesome/

#Configure sddm
doas systemctl enable sddm.service

sudo chsh -s "$(which zsh)"
chsh -s "$(which zsh)"

doas reboot
