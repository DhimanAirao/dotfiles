#!/usr/bin/sh

sudo apt update

sudo apt install fonts-firacode micro \
    snapd bottom guake exa \
    zsh neovim kitty bat \
    diff-so-fancy ripgrep \
    tmux fzf nodejs npm

cp -v my_bottom.toml ~/.config/bottom/bottom.toml

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git "${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}"/themes/powerlevel10k

if [[ -f ~/.zshrc ]]; then
    sed -i -e '/ZSH_THEME/s,\".*,"powerlevel10k/powerlevel10k”,g' ~/.zshrc
fi

cp -v my_alias ~/.my_alias
sed -i '$a if [[ -f ~/.my_alias ]]; then\n	 source ~/.my_alias\nfi' ~/.zshrc

cp -v my_theme.micro ~/.config/micro/colorschemes/

mkdir ~/.config/nvim
cp -vr ../nvim/* ~/.config/nvim/

mkdir ~/.config/kitty/
cp -vr ../kitty/* ~/.config/kitty/

cp ../tmux_conf ~/.tmux.conf
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

wget -q https://gitlab.com/DhimanAirao/dotfiles/-/snippets/2515022/raw/main/spotify.desktop -O ../spotify.desktop
sudo cp ../spotify.desktop /usr/share/applications/spotify.desktop
sudo cp /opt/spotify/spotify.desktop
