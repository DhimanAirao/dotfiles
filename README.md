<div align=center>
<div align="center">
    <!--img src ="https://img.shields.io/badge/Awesomewm-6c5d87.svg?&style=for-the-badge&logo=Lua&logoColor=white"/-->
    <a target="_blank" href="https://archlinux.org/">
        <img src ="https://img.shields.io/badge/Arch%20Linux-1793D1.svg?&style=for-the-badge&logo=Arch Linux&logoColor=white"/>
    </a>
    <a target="_blank" href="https://ubuntu.com/">
        <img src ="https://img.shields.io/badge/Ubuntu-E95420?style=for-the-badge&logo=ubuntu&logoColor=white"/>
    </a>
    <a target="_blank" href="https://neovim.io/">
        <img src ="https://img.shields.io/badge/NeoVim-%2357A143.svg?&style=for-the-badge&logo=neovim&logoColor=white"/>
    </a>
    <a target="_blank" href="https://sw.kovidgoyal.net/kitty/">
        <img src ="https://img.shields.io/badge/kitty+zsh-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white"/>
    </a>
</div>
</div>

This is readme file for setting up Dhiman Airao like enviroment in your linux system for all
Distro's either using apt/pacman as package manager.

# Modules

# Setup

1. Clone this Repository

```shell
git clone https://gitlab.com/DhimanAirao/dotfiles ~/.config/dotfiles
cd ~/.config/dotfiles
```

2. Setup the Enviroment

   **For Arch User**

   ```shell
   cd setup
   chmod +x my_setup_arch.sh
   ./my_setup_arch
   ```

   **For Debian User**

   ```shell
   cd setup
   chmod +x my_setup_apt.sh
   ./my_setup_apt
   ```
